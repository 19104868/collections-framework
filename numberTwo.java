/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activity;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 2ndyrGroupA
 */
public class numberTwo {
    public static void main(String args[]){  
       shuffleNames();
}
    public static void shuffleNames(){
        ArrayList<String> mylist = new ArrayList<String>();
       mylist.add("Math");
       mylist.add("English");
       mylist.add("Filipino");
       mylist.add("Science");
       mylist.add("PE");  
       System.out.println("Original List : \n" + mylist);  
       Collections.shuffle(mylist);
       System.out.println("\nShuffled List : \n" + mylist);
    }
   
}